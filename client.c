#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <openssl/des.h>

void error(const char *msg);
void *send_messages(void *ptr);
void *receive_messages(void *ptr);

char keybuf[20];

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    // creating threads to handle send and receive messages
    pthread_t sender, receiver;
    int sender_connect, receiver_connect;


    char buffer[256];
    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }

    fgets(keybuf,9,stdin);
    
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
      error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    // copying the server addr from server to serv_addr
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    // assigning server port
    serv_addr.sin_port = htons(portno);
    
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
      error("ERROR connecting");


    // reservo espacio en memoria donde guardo el un descriptor del client socket para cada thread
    int *sendersocketptr = malloc(sizeof(int));
    int *receiversocketptr = malloc(sizeof(int));
    *sendersocketptr = sockfd;
    *receiversocketptr = sockfd;


    sender_connect = pthread_create( &sender, NULL, send_messages, sendersocketptr);
    receiver_connect = pthread_create( &receiver, NULL, receive_messages, receiversocketptr);

    if(sender_connect) {
      error("ERROR creating sender thread.");
    }

    if(receiver_connect) {
      error("ERROR creating receiver thread.");
    }
    pthread_join(sender, NULL);
    pthread_join(receiver, NULL);

    // closing the socket
    close(sockfd);
    return 0;
}

void error(const char *msg) {
  perror(msg);
  exit(0);
}



void *send_messages(void *ptr) {
  unsigned char *key= malloc(strlen(keybuf));
  DES_string_to_key(keybuf, (DES_cblock*)key);
  
  DES_key_schedule schedule;
  DES_set_key_unchecked((const_DES_cblock*)key, &schedule);

  char buffer[256];
  unsigned char* input;
  unsigned char* output;
  int socketfd, len;
  socketfd = *(int *) ptr;

  do {
    bzero(buffer,256);
    fgets(buffer,255,stdin);
    len = strlen((char*)buffer);
    input = malloc(len);
    output = malloc(len);
    memcpy(input, buffer, len);

    DES_ecb_encrypt((const_DES_cblock*)input, (DES_cblock*)output, &schedule, DES_ENCRYPT);  
    len = write(socketfd,output,strlen((char *)output));
    if (len < 0){
      error("ERROR writing to socket.");
    }
    free(input);
    input = NULL;
    free(output);
    output = NULL;
  } while(strcmp(buffer,"exit\n")!=0);
  return NULL;
}

void *receive_messages(void *ptr){
  unsigned char *key= malloc(strlen(keybuf));
  DES_string_to_key(keybuf, (DES_cblock*)key);

  DES_key_schedule schedule;
  DES_set_key_unchecked((const_DES_cblock*)key, &schedule);
  
  char buffer[256];
  unsigned char* input;
  unsigned char* output;
  int socketfd, len;
  socketfd = *(int *) ptr;

  while(strcmp(output,"exit\n")!=0) { 
    bzero(buffer,256);
    // reading 255 bytes from socket into buffer
    len = read(socketfd,buffer,255);
    
    input = malloc(len);
    output = malloc(len);
    memcpy(input, buffer, len);

    DES_ecb_encrypt((const_DES_cblock*)input, (DES_cblock*)output, &schedule, DES_DECRYPT);
    
    if (len < 0) error("ERROR reading from socket");	    
    printf("<< %s\n",output);

    free(input);
    input = NULL;
    free(output);
    output = NULL;
  }
  return NULL;
}
