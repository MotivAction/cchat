
/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <openssl/des.h>

void error(const char *msg);
void *send_messages(void *ptr);
void *receive_messages(void *ptr);

char keybuf[20];

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     // address size
     socklen_t clilen;
     //char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     //int n;

     // creating threads to handle send and receive messages
     pthread_t sender, receiver;
     int sender_connect, receiver_connect;
     
     
     // error if no port provided
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     fgets(keybuf,20,stdin);
     fprintf(stderr,"He leido la key\n");

     // AF_INET: Specifies the communications domain in which a socket is to be created. Internet address. 
     // SOCK_STREAM: Specifies the communications domain in which a socket is to be created.
     // 0: Specifies a particular protocol to be used with the socket. Specifying a protocol of 0 causes socket() to use an unspecified default protocol appropriate for the requested socket type. 
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     // The bzero() function shall place n zero-valued bytes in the area pointed to by serv_addr.
     bzero((char *) &serv_addr, sizeof(serv_addr));
     // gets the portnumber supplied by parameter
     portno = atoi(argv[1]);
     // addres type to internet address
     serv_addr.sin_family = AF_INET;
     // binds the socket to all the local interfaces
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     // configures the port passed as parameter
     serv_addr.sin_port = htons(portno);

     // binds the socket to the structure that we have created
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");

     // listen(socket, maxWaitingQeue)
     listen(sockfd,5);

     clilen = sizeof(cli_addr);
     /*
     char exitString[256];
     bzero(exitString, 256);
     exitString[256] = {'e','x','i','t','!','\0'};
     */
     newsockfd = accept(sockfd,
			(struct sockaddr *) &cli_addr, 
			&clilen);
     
     if (newsockfd < 0) 
       error("ERROR on connection.");

     // reservo espacio en memoria donde guardo el un descriptor del client socket para cada thread
     int *sendersocketptr = malloc(sizeof(int));
     int *receiversocketptr = malloc(sizeof(int));
     *sendersocketptr = newsockfd;
     *receiversocketptr = newsockfd;
     // creating sender thread and getting its output
     sender_connect = pthread_create( &sender, NULL, send_messages, sendersocketptr);
     receiver_connect = pthread_create( &receiver, NULL, receive_messages, receiversocketptr);

     if(sender_connect) {
       error("ERROR creating sender thread.");
     }

     if(receiver_connect) {
       error("ERROR creating receiver thread.");
     }
     /*       
     while(1) {
       bzero(buffer,256);
       // reading 255 bytes from socket into buffer
       n = read(newsockfd,buffer,255);
       if (n < 0) error("ERROR reading from socket");
	    
       printf("Here is the message: %s\n",buffer);

       
	 if(strcmp(buffer, exitString) == 0)
	 break;
       
       printf("Please enter your message: ");
       bzero(buffer,256);
       fgets(buffer,255,stdin);
       
       n = write(newsockfd,buffer,strlen(buffer));
       if (n < 0)
	 error("ERROR writing to socket.");
       
     }
     */

     pthread_join(sender, NULL);
     pthread_join(receiver, NULL);
     // closing client socket.
     close(newsockfd);

     close(sockfd);
     return 0; 
}


void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void *send_messages(void *ptr) {
  unsigned char *key = malloc(strlen(keybuf));
  DES_string_to_key(keybuf, (DES_cblock*)key);
  
  DES_key_schedule schedule;
  DES_set_key_unchecked((const_DES_cblock*)key, &schedule);

  char buffer[256];
  unsigned char* input;
  unsigned char* output;
  int socketfd, len;
  socketfd = *(int *) ptr;

  do {
    bzero(buffer,256);
    fgets(buffer,255,stdin);
    len = strlen((char*)buffer);
    input = malloc(len);
    output = malloc(len);
    memcpy(input, buffer, len);

    DES_ecb_encrypt((const_DES_cblock*)input, (DES_cblock*)output, &schedule, DES_ENCRYPT);  
    len = write(socketfd,output,strlen((char *)output));
    if (len < 0){
      error("ERROR writing to socket.");
    }
    free(input);
    input = NULL;
    free(output);
    output = NULL;
  } while(strcmp(buffer,"exit\n")!=0);
  return NULL;
}

void *receive_messages(void *ptr){
  unsigned char *key = malloc(strlen(keybuf));
  DES_string_to_key(keybuf, (DES_cblock*)key);

  DES_key_schedule schedule;
  DES_set_key_unchecked((const_DES_cblock*)key, &schedule);
  
  char buffer[256];
  unsigned char* input;
  unsigned char* output;
  int socketfd, len;
  socketfd = *(int *) ptr;

  while(strcmp(output,"exit\n")!=0) { 
    bzero(buffer,256);
    // reading 255 bytes from socket into buffer
    len = read(socketfd,buffer,255);
    
    input = malloc(len);
    output = malloc(len);
    memcpy(input, buffer, len);

    DES_ecb_encrypt((const_DES_cblock*)input, (DES_cblock*)output, &schedule, DES_DECRYPT);
    
    if (len < 0) error("ERROR reading from socket");      
    printf("<< %s\n",output);

    free(input);
    input = NULL;
    free(output);
    output = NULL;
  }
  return NULL;
}