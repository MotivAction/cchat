CC=gcc
CFLAGS=-pthread -lcrypto -lssl -g -fno-stack-protector -D_FORTIFY_SOURCE=0
SERVER=server.c
CLIENT=client.c
S_OUT=server.o
C_OUT=client.o

build: $(SERVER) $(CLIENT)
	$(CC) -o $(S_OUT) $(CFLAGS) $(SERVER)
	$(CC) -o $(C_OUT) $(CFLAGS) $(CLIENT)

clean:
	rm -f *.o core

rebuild: clean build
